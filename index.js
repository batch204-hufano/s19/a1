// console.log("Hello World")

let username, password, role;

function login() {
	username = prompt("Enter Your Username: ");
	password = prompt("Enter Your Password: ");
	role = prompt("Enter Your Role: ");

	if (username === "" || username === null) {
		alert("Username should not be empty!");
	};

	if (password === "" || password === null) {
		alert("Password should not be empty!");
	};

	if (role === "" || role === null) {
		alert("Role should not be empty!");
	} else {
		switch(role) {
			case "admin":
				alert("Welcome back to the class portal, admin!");
				break;
			case "teacher":
				alert("Thank you for logging in, teacher!");
				break;
			case "student":
				alert("Welcome to the class portal, student!");
				break;
			default:
				alert("Role out of range.");
		};
	};
};

login();

/***********************************************************/


function checkAverage (grade1, grade2, grade3, grade4) {
	let average = (grade1 + grade2 + grade3 + grade4) / 4;
	average = Math.round(average);

	if (average <= 74) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
	} else if (average >= 75 && average <= 79) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is D");
	} else if (average >= 80 && average <= 84) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is C");
	} else if (average >= 85 && average <= 89) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is B");
	} else if (average >= 90 && average <= 95) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A");
	} else if (average >= 96) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");
	}
};

checkAverage(71, 70, 73, 74);
checkAverage(75, 75, 76, 78);
checkAverage(80, 81, 82, 78);
checkAverage(84, 85, 87, 88);
checkAverage(89, 90, 91, 90);
checkAverage(91, 96, 97, 95);
checkAverage(91, 96, 97, 99);



